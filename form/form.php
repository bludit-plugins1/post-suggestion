<form method="post" action="">
    <div id="frontendMessage">
        <?php echo $this->frontendMessage(); ?>
    </div>
    <?php if(isset($this->success) && $this->success) { return; } ?>

    <div class="form-group with-tip">
        <input id="name" type="text" name="name" value="<?php echo ($this->senderName); ?>" placeholder="<?php echo $L->get('Your Name'); ?>" class="form-control" required>
        <span class="tip">Für Rückfragen, wird nicht veröffentlicht</span>
    </div>

    <div class="form-group lastname">
        <input id="lastname" type="text" autocomplete="off" name="lastname" value="" placeholder="" class="form-control">
    </div>

    <div class="form-group with-tip">
        <input id="email" type="email" name="email" value="<?php echo ($this->senderEmail); ?>" placeholder="<?php echo $L->get('Your Email'); ?>" class="form-control" required>
        <span class="tip">Für Rückfragen, wird nicht veröffentlicht</span>
    </div>

    <div class="form-group with-tip">
        <input id="title" type="text" name="title" value="<?php echo ($this->title); ?>" placeholder="<?php echo $L->get('Title'); ?>" class="form-control" maxlength="100" required>
        <span class="tip">max. 100 Zeichen</span>
    </div>

    <div class="form-group with-tip">
        <textarea id="content" maxlength="300" name="description" placeholder="<?php echo $L->get('Description'); ?>" class="form-control" rows="6" required><?php echo ($this->description); ?></textarea>
        <span class="tip">max. 300 Zeichen</span>
    </div>
    <br>
    <button id="submit" name="submit" type="submit" class="btn btn-primary"><?php echo $L->get('Send'); ?></button>

</form>
