<?php

class pluginPostSuggestion extends Plugin{

    protected $senderName;
    protected $senderLastName;
    protected $senderEmail;
    protected $title;
    protected $description;

    protected $success = false;
    protected $errorMessage = '';

    public function init()
    {
        $this->dbFields = array(
            'webhookUrlForm'=>'public',
            'webhookUrlList' => 'artikelvorschlaege',
            'webhookUrlConfirm' => 'email-confirm',
            'suggestions' => '',
            'confirm-waiting' => '',
            'confirmed' => '',
            'sidebar-title' => 'Artikelvorschläge',
            'sidebar-limit' => 5,
            'email' => '',
            'name' => '',
            'smtphost' => '',
            'smtpport' => '',
            'smtpencryption' => '',
            'username' => '',
            'password' => '',
        );
    }

    public function form()
    {
        global $L;
        $html = '<div>';
        $html .= '<label>'.$L->get('Webhook URL Form').'</label>';
        $html .= '<input name="webhookUrlForm" class="form-control" type="text" value="'.$this->getValue('webhookUrlForm').'">';
        $html .= '</div>';

        $html .= '<div>';
        $html .= '<label>'.$L->get('Webhook URL List').'</label>';
        $html .= '<input name="webhookUrlList" class="form-control" type="text" value="'.$this->getValue('webhookUrlList').'">';
        $html .= '</div>';

        $html .= '<div>';
        $html .= '<label>'.$L->get('Sidebar Title').'</label>';
        $html .= '<input name="sidebar-title" class="form-control" type="text" value="'.$this->getValue('sidebar-title').'">';
        $html .= '</div>';

        $html .= '<div>';
        $html .= '<label>'.$L->get('Sidebar Limit').'</label>';
        $html .= '<input name="sidebar-limit" class="form-control" type="number" value="'.$this->getValue('sidebar-limit').'">';
        $html .= '</div>';

        
        $html .= Bootstrap::formInputText(array(
            'name' => 'smtphost',
            'label' => $L->get('SMTP Host'),
            'value' => $this->getValue('smtphost')
        ));

        // smtp port
        $html .= Bootstrap::formInputText(array(
            'name' => 'smtpport',
            'label' => $L->get('SMTP Port'),
            'class' => 'short-input',
            'value' => $this->getValue('smtpport')
        ));

        // smtp encryption
        $html .= Bootstrap::formSelect(array(
            'name' => 'smtpencryption',
            'label' => $L->get('SMTP Encryption'),
            'class' => 'short-input',
            'options' => array(
                false => $L->get('deactivate'),
                'starttls' => $L->get('STARTTLS'),
                'smtps' => $L->get('SSL/TLS')
            ),
            'selected' => $this->getValue('smtpencryption')
        ));

        // smtp username
        $html .= Bootstrap::formInputText(array(
            'name' => 'username',
            'label' => $L->get('SMTP Username'),
            'value' => $this->getValue('username')
        ));

        // smtp password
        $html .= Bootstrap::formInputText(array(
            'name' => 'password',
            'type' => 'password',
            'label' => $L->get('SMTP Password'),
            'value' => $this->getValue('password')
        ));

                // email receiver
                $html .= Bootstrap::formInputText(array(
                    'name' => 'email',
                    'label' => $L->get('Your Email'),
                    'value' => $this->getValue('email'),
                    'tip' => $L->get('This is the address you want the email to be sent TO.')
                ));
        
                // email receiver name
                $html .= Bootstrap::formInputText(array(
                    'name' => 'name',
                    'label' => $L->get('Email Name'),
                    'value' => $this->getValue('name'),
                    'tip' => $L->get('This is used for email receiver name and in CC email for sender name.')
                ));

        return $html;
    }


    public function siteHead()
    {
        if ($this->webhook($this->getValue('webhookUrlForm'))) {
            $html = '<link rel="stylesheet" href="' . $this->htmlPath() . 'form' . DS . 'form.css">' . PHP_EOL;
            return $html;
        }

    }

    public function pageBegin(){
        if ($this->webhook($this->getValue('webhookUrlConfirm'))) {
            $html = '<div class="alert alert-danger">Es ist ein Fehler aufgetreten.</div>';
            $hash = $_GET['hash'];
            $confirmWait = $this->db['confirm-waiting'];
            if (!is_array($confirmWait)) {
                $confirmWait = [];
            }
            if(isset($confirmWait[$hash])) {
                unset($this->db['confirm-waiting'][$hash]);
                $this->save();
                $confirmed = $this->db['confirmed'];
                if (!is_array($confirmed)) {
                    $confirmed = [];
                }
                $confirmed[$hash] = array(
                    'date' => date('Y-m-d H:i:s'),
                    'voted' => []
                );
                $this->db['confirmed'] = $confirmed;
                $this->save();
                $html = '<div class="alert alert-success">Deine E-Mail-Adresse wurde erfolgreich bestätigt.</div>';
            }

            return $html;
        }
    }


    // Load contact form
    public function pageEnd()
    {
        global $L;
        if ($this->webhook($this->getValue('webhookUrlList'))) {
            include __DIR__ . DS . 'list' . DS . 'list.php';
            return;
        }

        if (!$this->webhook($this->getValue('webhookUrlForm'))) {
            return;
        }
        $this->includeForm();
    }


    private function includeForm()
    {
        global $page, $security, $L;
        include(__DIR__ . DS . 'form' . DS . 'form.php');
    }

    private function validate()
    {
        global $L;
        $errors = [];

        if (trim($this->senderName) === '')
            array_push($errors, $L->get('Please enter your name'));

        if (trim($this->senderLastName) !== '')
            array_push($errors, $L->get('Something went wrong'));

        if (trim($this->senderEmail) === '' || !filter_var($this->senderEmail, FILTER_VALIDATE_EMAIL))
            array_push($errors, $L->get('Please enter a valid email address'));

        if (trim($this->title) === '')
            array_push($errors, $L->get('Please enter a title'));

        if (trim($this->description) === '')
            array_push($errors, $L->get('Please enter a short description'));

        return $errors;
    }

    public function frontendMessage()
    {
        global $L;
        if (isset($this->success) && $this->success) {
            $html = '<div class="alert alert-success">Vielen Dank. Wir werden deinen Vorschlag in Kürze prüfen.</div>' . "\r\n";
        } elseif (isset($this->errorMessage) && $this->errorMessage){
            $html = '<div class="alert alert-danger">' . rtrim($this->errorMessage, '<br>') . '</div>' . "\r\n";
        } else {
            $html = '';
        }
        return $html;
    }


    public function beforeAll()
    {
        global $url;

        //$this->cleanupConfirmWaitOlderThanOneHour();
        //$this->cleanupConfirmedOlderThanOneWeek();

        if($this->webhook('pluginPostSuggestion-form-confirm-mail')){
            header('Content-type: application/json');
            $path = __DIR__ . DS . '..'. DS . '..'. DS . 'bl-content'.DS;
            $date = date('[Y-m-d H:i:s]');      
            copy($path.'databases/plugins/post-suggestion/db.php', $path.'databases/plugins/post-suggestion/db_'.date('Ymd_His').'.php');
            file_put_contents($path.'workspaces'.DS.'post-suggestion/log.log' , $date. ' form-confirm-mail '. print_r($_POST,true). PHP_EOL, FILE_APPEND);
            $this->handleSendConfirmMail();
            exit(0);
        }


        if($this->webhook('pluginPostSuggestion-form-vote')){
            header('Content-type: application/json');
            $path = __DIR__ . DS . '..'. DS . '..'. DS . 'bl-content'.DS;
            $date = date('[Y-m-d H:i:s]');    
            copy($path.'databases/plugins/post-suggestion/db.php', $path.'databases/plugins/post-suggestion/db_'.date('Ymd_His').'.php');
            file_put_contents($path.'workspaces'.DS.'post-suggestion/log.log' , $date. ' form-vote '. print_r($_POST,true). PHP_EOL, FILE_APPEND);
            $this->handleVote();
            exit(0);
        }

        if (!$this->webhook($this->getValue('webhookUrlForm'))) {
            return;
        }
        
        $path = __DIR__ . DS . '..'. DS . '..'. DS . 'bl-content'.DS;
        $date = date('[Y-m-d H:i:s]');    
        copy($path.'databases/plugins/post-suggestion/db.php', $path.'databases/plugins/post-suggestion/db_'.date('Ymd_His').'.php');
        file_put_contents($path.'workspaces'.DS.'post-suggestion/log.log' , $date. ' webhookUrlForm '. print_r($_POST,true). PHP_EOL, FILE_APPEND);

        if (isset($_POST['submit'])) {
            global $L;
            $this->readPost();
            $errors = $this->validate();
            if (!empty($errors)) {
                foreach ($errors as $value) {
                    $this->errorMessage .= $value . '<br>';
                }
            }


            // stop if error
            if ($this->errorMessage) {
                $this->success = false;
                return;
            }
            $suggestions = $this->db['suggestions'];
            if (!is_array($suggestions)) {
                $suggestions = [];
            }
            if(isset($suggestions[md5($this->title)])) {
                return;
            }
            $suggestions[md5($this->title)] = array(
                'name' => $this->senderName,
                'email' => $this->senderEmail,
                'title' => $this->title,
                'description' => $this->description,
                'date' => date('Y-m-d H:i:s'),
                'status' => 'pending',
                'voting' => 0
            );
            $this->db['suggestions'] = $suggestions;
            $this->save();
            $this->success = true;

        }
    }


    private function readPost()
    {
        if (isset($_POST['name'])) {
            $this->senderName = trim(strip_tags($_POST['name']));
        }
        if (isset($_POST['lastname'])) {
            $this->senderLastName = trim(strip_tags($_POST['lastname']));
        }
        if (isset($_POST['email'])) {
            $this->senderEmail = trim(preg_replace("/[^0-9a-zA-ZäöüÄÖÜÈèÉéÂâáÁàÀíÍìÌâÂ@ \-\+\_\.]/", " ", $_POST['email']));
        }
        if (isset($_POST['title'])) {
            $this->title = trim(strip_tags($_POST['title']));
        }
        if (isset($_POST['description'])) {
            $this->description = trim(strip_tags($_POST['description']));
        }

    }

    public function adminSidebar()
    {
        $pendingCount = count($this->getPendingSuggestions());
        $pending = '';
        if($pendingCount > 0) {
            $pending = ' <span class="badge badge-primary badge-pill">'.$pendingCount.'</span>';
        }
        $html = '<a class="nav-link" href="'.HTML_PATH_ADMIN_ROOT.'plugin/pluginPostSuggestion">Artikelvorschläge '.$pending.'</a>';
        return $html;
    }

    protected function getPendingSuggestions()
    {
        $suggestions = $this->db['suggestions'];
        $pendingSuggestions = [];
        foreach ($suggestions as $key => $suggestion) {
            if ($suggestion['status'] === 'pending') {
                $pendingSuggestions[$key] = $suggestion;
            }
        }
        return $pendingSuggestions;
    }

    public function getApprovedSuggestions($limit = false)
    {
        $suggestions = $this->db['suggestions'];
        uasort($suggestions, function($a, $b) {
            // Compare the "voting" field in descending order
            $votingComparison = $b['voting'] <=> $a['voting'];
            if ($votingComparison !== 0) {
                return $votingComparison;
            }
        
            // If "voting" is equal, compare the "date" field in descending order
            return $b['date'] <=> $a['date'];
        });
        $approvedSuggestions = [];
        $x=0;
        foreach ($suggestions as $key => $suggestion) {
            $x++;
            if ($suggestion['status'] === 'accepted') {
                $approvedSuggestions[$key] = $suggestion;
            }
            if($x>=$limit && $limit !== false) {
                break;
            }
        }
        return $approvedSuggestions;
    }


    public function getDoneSuggestions($limit = false)
    {
        $suggestions = $this->db['suggestions'];
        uasort($suggestions, function($a, $b) {
            // Compare the "voting" field in descending order
            $votingComparison = $b['voting'] <=> $a['voting'];
            if ($votingComparison !== 0) {
                return $votingComparison;
            }

            // If "voting" is equal, compare the "date" field in descending order
            return $b['date'] <=> $a['date'];
        });
        $approvedSuggestions = [];
        $x=0;
        foreach ($suggestions as $key => $suggestion) {
            $x++;
            if ($suggestion['status'] === 'done') {
                $approvedSuggestions[$key] = $suggestion;
            }
            if($x>=$limit && $limit !== false) {
                break;
            }
        }
        return $approvedSuggestions;
    }

    public function adminView(){
        global $L;
        if ($_POST['type']==='delete') {
            $this->deleteSuggestion($_POST['key']);
        }
        if ($_POST['type']==='accept') {
            $suggestions = $this->db['suggestions'];
            if(isset($suggestions[$_POST['key']])) {
                $suggestions[$_POST['key']]['status'] = 'accepted';
                $this->db['suggestions'] = $suggestions;
                $this->save();
            }
        }
        if ($_POST['type']==='done') {
            $suggestions = $this->db['suggestions'];
            if(isset($suggestions[$_POST['key']])) {
                $suggestions[$_POST['key']]['status'] = 'done';
                $this->db['suggestions'] = $suggestions;
                $this->save();
            }
        }
        include __DIR__ . DS . 'admin'.DS.'admin.php';
    }

    private function deleteSuggestion($key)
    {
        $suggestions = $this->db['suggestions'];
        if(isset($suggestions[$key])) {
            unset($suggestions[$key]);
            $this->db['suggestions'] = $suggestions;
            $this->save();
            return true;
        }
        return false;
    }
    public function siteSidebar()
    {
        $html  = '<div class="plugin plugin-pages">';
        if ($this->getValue('sidebar-title')) {
            $html .= '<h2 class="plugin-label">'.$this->getValue('sidebar-title').'</h2>';
        }
        $html .= '<div class="plugin-content"><ul>';
        $suggest = $this->getApprovedSuggestions($this->getValue('sidebar-limit'));
        foreach ($suggest as $key => $suggestion) {
            $html .= '<li><a href="'.$this->getValue('webhookUrlList').'#'.$key.'">'.$suggestion['title'] . '</a></li>';
        }
        $html .= '<li><a href="'.$this->getValue('webhookUrlList').'">...</a></li>';
        $html .= '</ul><a class="btn btn-outline-primary" href="/'.$this->getValue('webhookUrlForm').'">Vorschlag einreichen</a></div>';
        $html .= '</div><br>';

        return $html;
    }

    protected function handleSendConfirmMail()
    {
        $hash = $this->generateHash($_POST['email']);
        $confirmWait = $this->db['confirm-waiting'];
        if (!is_array($confirmWait)) {
            $confirmWait = [];
        }

        $confirmWait[$hash] = array(
            'date' => date('Y-m-d H:i:s')
        );
        $this->db['confirm-waiting'] = $confirmWait;
        $this->save();

        $this->sendEmail($_POST['email'], 'Bitte bestätige deine E-Mail-Adresse', $this->getConfirmMailBody($hash));
        $result = new stdClass();
        $result->success = true;
        echo json_encode($result);
    }

    private function sendEmail($receiverMail, $subject, $body)
    {
        try {
            require __DIR__ . DS . 'phpmailer' . DS . 'PHPMailerAutoload.php';
            $mail = new PHPMailer();
         
            if ($this->getValue('smtphost')) {
                $mail->isSMTP();
                $mail->Host = $this->getValue('smtphost');
                $mail->Port = $this->getValue('smtpport');
                switch ($this->getValue('smtpencryption')) {
                    case 'starttls':
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
                        break;
                    case 'smtps':
                        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
                        break;
                }

                $mail->SMTPAuth = true;
                $mail->Username = $this->getValue('username');
                $mail->Password = html_entity_decode($this->getValue('password')); // Function is needed if password contains special characters like '&'
            }
            
            $senderEmail = $this->getValue('email');
            $senderName = $this->getValue('name');
            $mail->setFrom($senderEmail, $senderName);
            $mail->addAddress($receiverMail);


            $mail->CharSet = CHARSET;
            $mail->Subject = $subject;
            $mail->isHTML(true);
            $mail->Body = $body;
            return $mail->send();

        } catch (Exception $e) {
            $errorMsg = "E-Mail konnte nicht gesendet werden";
            $result = new stdClass();
            $result->error = $errorMsg;
            echo json_encode($result);
            exit(0);
        }
    }

    private function getConfirmMailBody($hash)
    {
        global $site;
        $appUrl = (string)$site->url();
        $link = $appUrl .'/'. $this->getValue('webhookUrlConfirm') . '?hash=' . $hash;
        $html = "Hallo,";
        $html .= '<p>um abstimmen zu können, bestätige bitte deine E-Mail-Adresse, indem du auf den folgenden Link klickst:</p>';
        $html .= '<p><a href="'.$link.'">'.$link.'</a></p>';
        $html .= '<p>Der Link ist 60 Minuten lang gültig. Nach der Bestätigung kannst du eine Woche lang abstimmen, solange bleibt deine E-Mail-Adresse bei uns gespeichert.</p>';
        $html .= '<p>Freie Grüße,<br> dein GNU/Linux.ch Team</p>';
        return $html;
    }

    private function generateHash($email)
    {
        global $site;
        $appUrl = md5((string)$site->url());
        return hash('sha256', $email . $appUrl);
    }

    protected function cleanupConfirmWaitOlderThanOneHour(){
        $confirmWait = $this->db['confirm-waiting'];
        if (!is_array($confirmWait)) {
            $confirmWait = [];
        }
        foreach ($confirmWait as $key => $value) {
            $date = strtotime($value['date']);
            $nowMinusOneHour = strtotime('-1 hour');
            if ($date < $nowMinusOneHour) {
                unset($confirmWait[$key]);
            }
        }
        $this->db['confirm-waiting'] = $confirmWait;
        $this->save();
    }

    protected function cleanupConfirmedOlderThanOneWeek(){
        $confirmed = $this->db['confirmed'];
        if (!is_array($confirmed)) {
            $confirmed = [];
        }
        foreach ($confirmed as $key => $value) {
            $date = strtotime($value['date']);
            $nowMinusOneWeek = strtotime('-1 week');
            if ($date < $nowMinusOneWeek) {
                unset($confirmed[$key]);
            }
        }
        $this->db['confirmed'] = $confirmed;
        $this->save();
    }

    protected function handleVote(){
        $key = $_POST['key'];
        $email = $_POST['email'];
        $hash = $this->generateHash($email);
        $confirmed = $this->db['confirmed'];
        if (!is_array($confirmed)) {
            $confirmed = [];
        }
        if(isset($confirmed[$hash])) {
            $voted = $confirmed[$hash]['voted'];
            if (!in_array($key, $voted)) {
                $suggestions = $this->db['suggestions'];
                $suggestions[$key]['voting']++;
                $this->db['suggestions'] = $suggestions;
                $this->save();
                $confirmed[$hash]['voted'][] = $key;
                $this->db['confirmed'] = $confirmed;
                $this->save();
                $result = new stdClass();
                $result->success = true;
            }else{
                $result = new stdClass();
                $result->error = 'Du hast bereits für diesen Vorschlag abgestimmt';
            }
        }else{
            $result = new stdClass();
            $result->error = 'Bitte bestätige zuerst deine E-Mail-Adresse <button type="button" id="confirmMail" class="btn btn-outline-primary confirmMailBtn">E-Mail-Adresse bestätigen</button>';

        }
        echo json_encode($result);
    }
}