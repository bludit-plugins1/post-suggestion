Admin Tag Charts Plugin
================================
Post Suggestion Plugin for the [**Bludit CMS**](https://www.bludit.com/)

Plugin to add the ability to make suggestions for posts

License
-------
This is open source software licensed under the [MIT license](https://tldrlegal.com/license/mit-license).
