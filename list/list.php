<script>
    confirmMail = function(){
    if(document.getElementById('modalForm').reportValidity()){
        
        $.ajax({
            type: "POST",
            url: "/pluginPostSuggestion-form-confirm-mail",
            data: {
                email: $('#email').val(),
                tokenCSRF: "<?php echo Session::get('tokenCSRF'); ?>"
            },
            success: function(data){
                console.log(data);
                if(data.success == true){
                    showMessage('E-Mail mit Bestätigungslink gesendet','success');
                } else {
                    if(data.error){
                        showMessage(data.error,'danger');
                    } else {
                        showMessage('Es ist ein Fehler aufgetreten','danger');
                    }
                }
            },
            error: function(data){
                showMessage('Es ist ein Fehler aufgetreten','danger');
            }
        });
    }

}
</script>

<?php
$suggestions = $this->getApprovedSuggestions();
foreach ($suggestions as $key => $suggestion) {
    echo '<a id="'.$key.'"></a><div class="card mb-3" id="box-'.$key.'">
                        <div class="card-body">
                            <h5 class="card-title">'.$suggestion['title'].'</h5>
                            <p class="card-text">'.$suggestion['description'].'</p>
                            <a href="#" data-bs-toggle="modal" data-bs-target="#jsVoteModal" data-key="'.$key.'" class="btn btn-outline-primary openVoteModal">&#x1F44D;</a>
                            &nbsp; '.$suggestion['voting'].' Stimmen
                        </div>
                    </div>';
}
?>

<!-- Modal -->
<div class="modal fade" id="jsVoteModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    <form id="modalForm" onkeydown="return event.key != 'Enter';">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="exampleModalLabel">Für einen Vorschlag abstimmen</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        Um Manipulationen zu vermeiden, dürfen nur bestätigte E-Mail-Adressen abstimmen.<br><br>
        <b>Hinweis:</b> Wir speichern bestätigte E-Mail-Adressen <b>1 Woche lang</b> und <b>ausschließlich zu diesem Zweck</b>. Danach muss die Bestätigung erneut durchgeführt werden.
        <br><br>
            <input type="hidden" name="key" id="jsVoteModalKey">
            <div class="mb-3">
                <input type="email" class="form-control" placeholder="E-Mail-Adresse" id="email" name="email" required>
            </div>
            <div id="message"></div>
      </div>
      <div class="modal-footer">
        <button type="button" id="vote" class="btn btn-primary">&#x1F44D; Abstimmen</button>
      </div>
    </form>
    </div>
  </div>
</div>



<script>
    function highlightCard() {
        // reset all elements with class 'card'
        var elements = document.getElementsByClassName('card mb-3');
        for (var i = 0; i < elements.length; i++) {
            elements[i].style.borderStyle = 'solid';
            elements[i].style.borderWidth = '1px';
        }
        var hash = window.location.hash;
        if (hash) {
            var element = document.getElementById('box-' + hash.substring(1));
            if (element) {
                element.style.borderStyle = 'outset';
                element.style.borderWidth = '3px';
            }
        }

    }
    window.onhashchange = highlightCard;
    var hash = window.location.hash;
    if (hash) {
        highlightCard();
    }
    
    
</script>
<script type="text/javascript">

var key = "";

$('.openVoteModal').click(function(){
    key = $(this).data('key');
});



// 

$('#vote').click(function(){
    if(document.getElementById('modalForm').reportValidity()){
        
        $.ajax({
            type: "POST",
            url: "/pluginPostSuggestion-form-vote",
            data: {
                email: $('#email').val(),
                key: key,
                xy: "yx",
                tokenCSRF: "<?php echo Session::get('tokenCSRF'); ?>"
            },
            success: function(data){
                if(data.success == true){
                    window.location.reload();
                } else {
                    if(data.error){
                        showMessage(data.error,'danger');
                        $('.confirmMailBtn').click(function(){
                            confirmMail();
                        })
                    } else {
                        showMessage('Es ist ein Fehler aufgetreten','danger');
                    }
                }
            },
            error: function(data){
                showMessage('Es ist ein Fehler aufgetreten','danger');
            }
        });
    }

});

// show a message in #message for 4 seconds
function showMessage(message, type) {
    msgHtml = document.createElement('div');
    msgHtml.id = Math.random().toString(36).substring(7);
    msgHtml.classList.add('alert');
    msgHtml.classList.add('alert-'+type);
    msgHtml.innerHTML = message;
    $(msgHtml).hide();
    $('#message').append(msgHtml);
    $(msgHtml).fadeIn(500);
    $(msgHtml).delay(5000).fadeOut(1000);
}

</script>