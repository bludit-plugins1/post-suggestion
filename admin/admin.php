<h2 class="mt-0 mb-3">Artikelvorschläge</h2>

<ul class="nav nav-pills" data-handle="tabs">
    <li class="nav-item">
        <a id="pending-tab" href="#pending" class="nav-link nav-pending active" data-toggle="tab">
            Ausstehend </a>
    </li>
    <li class="nav-item">
        <a id="approved-tab" href="#approved" class="nav-link nav-approved" data-toggle="tab">
            Genehmigt            </a>
    </li>
    <li class="nav-item">
        <a id="approved-tab" href="#done" class="nav-link nav-done" data-toggle="tab">
            Erledigt            </a>
    </li>
</ul>

<div class="tab-content">
    <div id="pending" class="tab-pane active">
        <br>
        <?php
            $suggestions = $this->getPendingSuggestions();
            foreach ($suggestions as $key => $suggestion) {
                echo '<div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">'.$suggestion['title'].'</h5>
                            <p class="card-text">'.$suggestion['description'].'</p>
                            <p class="card-text"><small class="text-muted smaller-font-size">'.$suggestion['name'].' - '.$suggestion['email'].'</small></p>
                            <form method="post" style="display: inline" action="/admin/plugin/pluginPostSuggestion">
                                <input type="hidden" name="type" value="accept">
                                <input type="hidden" name="key" value="'.$key.'">
                                <input type="hidden" name="tokenCSRF" value="'.Session::get('tokenCSRF').'">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Genehmigen</button>
                            </form>
                            <a href="#" class="btn btn-danger deleteSuggestionButton" data-toggle="modal" data-target="#jsdeleteSuggestionModal" data-key="'.$key.'"><i class="fa fa-trash"></i>'.$L->g('Delete').'</a>
                        </div>
                    </div>';
            }
        ?>
    </div>
    <div id="approved" class="tab-pane">
        <br>
        <?php
        $suggestions = $this->getApprovedSuggestions();
        foreach ($suggestions as $key => $suggestion) {
            echo '<div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">'.$suggestion['title'].'</h5>
                            <p class="card-text">'.$suggestion['description'].'</p>
                            <p class="card-text"><small class="text-muted smaller-font-size">'.$suggestion['name'].' - '.$suggestion['email'].'</small></p>
                            <a href="#" class="btn btn-danger deleteSuggestionButton" data-toggle="modal" data-target="#jsdeleteSuggestionModal" data-key="'.$key.'"><i class="fa fa-trash"></i>'.$L->g('Delete').'</a>
                            <form method="post" style="display: inline" action="/admin/plugin/pluginPostSuggestion">
                                <input type="hidden" name="type" value="done">
                                <input type="hidden" name="key" value="'.$key.'">
                                <input type="hidden" name="tokenCSRF" value="'.Session::get('tokenCSRF').'">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> erledigt</button>
                            </form>
                        </div>
                    </div>';
        }
        ?>
    </div>
    <div id="done" class="tab-pane">
        <br>
        <?php
        $suggestions = $this->getDoneSuggestions();
        foreach ($suggestions as $key => $suggestion) {
            echo '<div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">'.$suggestion['title'].'</h5>
                            <p class="card-text">'.$suggestion['description'].'</p>
                            <p class="card-text"><small class="text-muted smaller-font-size">'.$suggestion['name'].' - '.$suggestion['email'].'</small></p>
                            <a href="#" class="btn btn-danger deleteSuggestionButton" data-toggle="modal" data-target="#jsdeleteSuggestionModal" data-key="'.$key.'"><i class="fa fa-trash"></i>'.$L->g('Delete').'</a>
                        </div>
                    </div>';
        }
        ?>
    </div>
</div>

<?php

echo Bootstrap::modal(array(
    'buttonPrimary'=>$L->g('Delete'),
    'buttonPrimaryClass'=>'btn-danger deleteSuggestionModalAcceptButton',
    'buttonSecondary'=>$L->g('Cancel'),
    'buttonSecondaryClass'=>'btn-link',
    'modalTitle'=>$L->g('Delete'),
    'modalText'=>$L->g('Wirklich löschen?'),
    'modalId'=>'jsdeleteSuggestionModal'
));

echo <<<SCRIPT
<script type="text/javascript">
$(document).ready(function() {
    
	var key = false;

	$(".deleteSuggestionButton").on("click", function() {
		key = $(this).data('key');
	});    
    
    $(".deleteSuggestionModalAcceptButton").on("click", function() {
            var form = jQuery('<form>', {
                'action': HTML_PATH_ADMIN_ROOT+'plugin/pluginPostSuggestion',
                'method': 'post',
                'target': '_top'
		}).append(jQuery('<input>', {
			'type': 'hidden',
			'name': 'tokenCSRF',
			'value': tokenCSRF
		}).append(jQuery('<input>', {
			'type': 'hidden',
			'name': 'key',
			'value': key
		}).append(jQuery('<input>', {
			'type': 'hidden',
			'name': 'type',
			'value': 'delete'
		}))));

    
            form.hide().appendTo("body").submit();
        });
});
</script>
SCRIPT;
